# How to setup

The following instructions explain how to setup this project for Linux environment after clone it
from this repository.

## Requirements

This project was made with Django 3.0 and dependencies are managed in a virtual environment.
To create the virtual environment, you need to install Python 3 and Pipenv first. To manage the project, you need Git installed.

The following versions are in used :

- Python 3.8
- [Pipenv 2018.11.26](https://pypi.org/project/pipenv/)
- Git

It's better if you install Pipenv globally to get the command anywhere on your machine and in virtual environnements. In order to do it, use this command :
`sudo -H pip3 install -U pipenv`

## Clone the project

To clone this repository in your local machine, create a new folder and execute the following command inside :
`git clone git@gitlab.com:diet-optimizer/django-graphql-auth.git`

## Virtual Environment

After installing Pipenv you should have `pipenv` command enable or, if it's not in path you can try `python3 -m pipenv`.
To create or activate the virtual environment, go to your project folder (django-graphql-auth/django-graphql-auth) and
execute the following command in a prompt :

`pipenv shell`

All commands must be executed in an activated virtual environment.
If you want to exit the environment, execute :

`exit`

## Install project depedencies

In your project folder, after activating your virtual environment, execute the
following command in order to install project dependencies :

`pipenv install`

It must generate a Pipfile.lock or if it's already here, it use Pipfile.lock to install packages.
Project dependencies contains the following packages :

- django
- graphene-django (to create a graphql api)
- django-graphql-jwt (to allow graphql authentication with jwt)
- django-graphql-auth (to simplify authentication )

And the following dev packages :

- black (for formatting)
- django-extensions (for the shell_plus)

Then Django and all packages dependencies are installed.

## Debug

To open a iPython shell with all the models loaded :

`./manage.py shell_plus`

Note that you must have the django-extensions package installed

## Migrations

To create an up-to-date migration file :

`./manage.py makemigrations`

To apply the database migrations :

`./manage.py migrate`

## Run

Now, you can run the project with the server by executing this command :

`./manage.py runserver`

- Go to http://localhost:8000/graphql to use the GraphiQL debugger

## Setup the Django administration

In order to use administration site, you have to create an admin by executing the
following command :

`./manage.py createsuperuser`
